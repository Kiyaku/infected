using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Cloud : MonoBehaviour {
	private float maxRandom = 100f;
	private List<Vector3> verticesList = new List<Vector3>();
	private List<Vector3> tempVerticesList = new List<Vector3>();
	
	private float speed = 0.01f;
	private float angle = 0;
	private MeshFilter meshFilter;
	private Mesh cloudMesh;
	
	
	void Start() {
		meshFilter = GetComponent<MeshFilter>();
		cloudMesh = meshFilter.mesh;
		speed = Random.Range(1f, 2f);
		randomizeMesh();
	}
	
	
	private void randomizeMesh() {
		verticesList.AddRange(cloudMesh.vertices);
		
		for(int i = 0; i < verticesList.Count; i++) {
			verticesList[i] += new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
		}
		
		cloudMesh.vertices = verticesList.ToArray();
		meshFilter.mesh = cloudMesh;
		
		StartCoroutine(wobbleMesh());
	}
	
	
	IEnumerator wobbleMesh() {
		while(true) {
			tempVerticesList.Clear();
			
			for(int i = 0; i < verticesList.Count; i++) {
				float tempVal = SimplexNoise.Noise.Generate(angle / 100f) * maxRandom;
				
				tempVerticesList.Add(Vector3.Slerp (verticesList[i], verticesList[i] + new Vector3(tempVal, tempVal, 0), Time.deltaTime * speed));
			}
			
			cloudMesh.vertices = tempVerticesList.ToArray();
			
			angle += Time.deltaTime * speed;
			
			yield return null;
		}
	}
}
