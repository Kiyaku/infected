using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CloudCluster : MonoBehaviour {
	public Transform CloudPrefab;
	public int LayerAmount = 4;
	
	private List<Transform> cloudCluster = new List<Transform>();
	
	
	void Start() {
		// Spawn particles
		for(int i = 0; i < LayerAmount; i++) {
			cloudCluster.Add((Transform)Instantiate (CloudPrefab, transform.position + new Vector3(0, 0, -(i / 5f)), transform.rotation));
			cloudCluster[i].parent = transform;
			cloudCluster[i].localScale *= transform.localScale.x - (((float)i / LayerAmount) * transform.localScale.x * 0.7f);
		}
	}
}
