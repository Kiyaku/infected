using UnityEngine;
using System.Collections;

public static class PlayerSkills {
	private static float strengthModifier = 1f;
	private static float maneveurability = 1f;
	private static int maxMinions = 1;	
	private static int health = 100;
	
	public static float HackModifier {
		get;
		private set;
	}
	
	public static float SpeedModifier {
		get;
		private set;
	}
	
	
	public static void Initialize() {
		HackModifier 	= 1f;
		SpeedModifier 	= 1f;
	}
}
