using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameManager : MonoBehaviour {
	public static GameManager ins;
	
	public Transform PlayerPtr;
	public Camera InterfaceCam;
	public Trojan TrojanPrefab;
	public Transform AntiVirusPrefab;
	public AudioClip SFXHurt;
	public AudioClip SFXShot;
	public AudioClip SFXAlarm;
	public List<AudioClip> HackSFX = new List<AudioClip>();
	
	public bool GameStarted = false;
	public bool PlayerAlive = true;
	
	public GUIText AlertText;
	
	private int maxTrojans = 0;
	private List<Trojan> TrojanList = new List<Trojan>();
	private List<HardDrive> availableDrives = new List<HardDrive>();
	private string[] trojanNames = {"C:\\", "D:\\" };
	
	private int dataGathered = 0;
	private bool playerDetected = false;
	
	
	void Awake() {
		ins = this;
		
		PlayerSkills.Initialize();
	}
	
	
	void Update() {
		if(GameStarted && PlayerAlive) {
			if(!Screen.lockCursor && Input.GetMouseButton(0))
				Screen.lockCursor = true;
			
			if(Input.GetKeyDown(KeyCode.Escape))
				Screen.lockCursor = false;
		}
	}
	
	
	public void PlayHitSound() {
		audio.PlayOneShot(SFXHurt);
	}
	
	public void PlayShotSound() {
		audio.PlayOneShot(SFXShot);
	}
	
	
	#region Resources
	public void GatherData(int amount) {
		dataGathered += amount;
	}
	#endregion
	
	
	public void DetectPlayer() {
		playerDetected = true;
		
		int avAmount = Random.Range(5, 10);
		
		for(int i = 0; i < avAmount; i ++) {
			float angle = i * (360f / avAmount);
			Vector3 spawnPos = PlayerPtr.position + new Vector3(Mathf.Sin(angle) * 25f, 3f, Mathf.Cos(angle) * 25f);
			spawnPos.y = 3;
			Instantiate(AntiVirusPrefab, spawnPos, Quaternion.identity);
		}
		
		StartCoroutine(alterPlayer());
	}
	
	
	private IEnumerator alterPlayer() {
		float timer = 0;
		
		audio.PlayOneShot(SFXAlarm);
		
		AlertText.enabled = true;
		
		while(timer < 10f) {
			if(Random.Range(0, 100) < 2) {
				float angle = Random.Range(0, 360);
				Vector3 spawnPos = PlayerPtr.position + new Vector3(Mathf.Sin(angle) * 25f, 3f, Mathf.Cos(angle) * 25f);
				spawnPos.y = 3;
				Instantiate(AntiVirusPrefab, spawnPos, Quaternion.identity);
			}
			
			AlertText.text = "- ALERT -\nLOOSING VIRUS IN " + System.String.Format("{0:0.0}", 10f - timer) + "S";
			timer += Time.deltaTime;
			yield return null;
		}
		
		LostPlayer();
	}
	
	
	public void LostPlayer() {
		playerDetected = false;
		AlertText.enabled = false;
		GetComponent<Trace>().StopTracing();
	}
	
	
	#region Trojan
	public void SpawnTrojan(Transform target) {
		if(TrojanList.Count < maxTrojans) {
			Trojan t = (Trojan)Instantiate (TrojanPrefab, target.position + new Vector3(0, 20, 0), Quaternion.identity);
			t.Name = trojanNames[TrojanList.Count];
			t.MyDrive = availableDrives[TrojanList.Count].transform;
			t.MyTarget = target;
			
			TrojanList.Add(t);
		}
	}
	
	
	public void AddHardDrive(HardDrive hd) {
		availableDrives.Add(hd);
		maxTrojans++;
	}
	
	
	public void ShowDriveInterface(HardDrive hd) {
		StartCoroutine(interactDrive(hd));
	}
	
	
	private IEnumerator interactDrive(HardDrive hd) {
		float timer = 0;
		
		transform.SendMessage("DrawInfoBox", hd.transform.position);
		
		while(Vector3.Distance(PlayerPtr.position, hd.transform.position) < 25) {
			if(!hd.Hacked) {
				transform.SendMessage("UpdateInfoText", "HARDDRIVE LOCKED");
				
				//if(Input.GetKey(KeyCode.E)) {
				if(hd.IsActive) {
					if(Random.Range(0, 100) < 10) {
						audio.PlayOneShot(HackSFX[Random.Range(0, HackSFX.Count)]);
					}
					
					if(timer > 0)
						transform.SendMessage("UpdateInfoText", "ANALYZING FILES\nCOMPLETION: " + (int)(timer * 100f) + "%");
					if(timer > 1)
						transform.SendMessage("UpdateInfoText", "ANALYZING FILES\nCOMPLETION: 100%\nDECRYPTING FILES\nCOMPLETION: " + (int)((timer - 1) * 100f) + "%");
					if(timer > 2)
						transform.SendMessage("UpdateInfoText", "ANALYZING FILES\nCOMPLETION: 100%\nDECRYPTING FILES\nCOMPLETION: 100%\nINJECTING OWN PATTERN\nCOMPLETION: " + (int)((timer - 2f) * 100f) + "%");
					
					transform.SendMessage("UpdateProgress", ((timer / 3f)));
					
					timer += Time.deltaTime * 0.2f * PlayerSkills.HackModifier;
				} else {
					transform.SendMessage("UpdateInfoText", "ERROR: ACCESS DENIED");
				}
				
				if(timer >= 3) {
					hd.HackDrive();
				}
			} else {
				transform.SendMessage("UpdateInfoText", "DR1V3 OV3RWR1TT3N\n\nH4XX");
			}
			
			yield return null;
		}
		
		transform.SendMessage("HideInfoBox");
	}
	#endregion
	
	
	#region Nodes
	public void ShowNodeInterface(Node node) {
		StartCoroutine(interactNode(node));
	}
	
	
	private IEnumerator interactNode(Node node) {
		float timer = 0;
		
		transform.SendMessage("DrawInfoBox", node.transform.position);
		
		while(Vector3.Distance(PlayerPtr.position, node.transform.position) < 16) {
			if(!node.IsActive) {
				transform.SendMessage("UpdateInfoText", "NODE OFFLINE");
				
				if(NodeManager.ins.IsNodeAvailable(node) && !playerDetected) {
					if(Random.Range(0, 100) < 1) {
						GetComponent<Trace>().StartTracing();
					}
					
					if(Random.Range(0, 100) < 10) {
						audio.PlayOneShot(HackSFX[Random.Range(0, HackSFX.Count)]);
					}
					
					transform.SendMessage("UpdateInfoText", "OVERWRITING KERNEL\n...\n...\n\nCOMPLETION " + (int)((timer / 5f) * 100f) + "%");
					transform.SendMessage("UpdateProgress", ((timer / 5f)));
					
					timer += Time.deltaTime * PlayerSkills.HackModifier;
				} else {
					transform.SendMessage("UpdateInfoText", "ERROR: ACCESS DENIED");
				}
			
				if(timer >= 5) {
					node.ActivateNode();
				}
			} else {
				transform.SendMessage("UpdateInfoText", "N0D3 0NL1N3\n\nH4XX");
				GetComponent<Trace>().StopTracing();
			}
			
			yield return null;
		}
		
		if(!playerDetected)
			GetComponent<Trace>().StopTracing();
		
		transform.SendMessage("HideInfoBox");
	}
	#endregion
	
	
	void OnGUI() {
		//GUI.Label(new Rect(10, 10, 200, 30), "Trojan Capacity: " + TrojanList.Count + "/" + maxTrojans, "Box");
		//GUI.Label(new Rect(10, 50, 200, 30), "Data gathered: " + dataGathered, "Box");
	}
}
