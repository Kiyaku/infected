using UnityEngine;
using System.Collections;

public class InfoBox : MonoBehaviour {
	public GUIText TextInfo;
	public GUIText TextInfoStatus;
	public GUITexture InfoBG;
	public GUITexture InfoProgress;
	public float MaxBarWidth;
	
	
	void Awake() {
		HideInfoBox();
	}
	
	
	public void DrawInfoBox() {
		StartCoroutine(fadeToAlpha(1));
	}
	
	
	public void HideInfoBox() {
		TextInfo.text = "";
		InfoProgress.pixelInset = new Rect(InfoProgress.pixelInset.x, InfoProgress.pixelInset.y, 0, InfoProgress.pixelInset.height);
		StartCoroutine(fadeToAlpha(0.3f));
	}
	
	
	public void UpdateProgress(float percent) {
		InfoProgress.pixelInset = new Rect(InfoProgress.pixelInset.x, InfoProgress.pixelInset.y, MaxBarWidth * percent, InfoProgress.pixelInset.height);
	}
	
	
	public void UpdateInfoText(string text) {
		InfoProgress.pixelInset = new Rect(InfoProgress.pixelInset.x, InfoProgress.pixelInset.y, 0, InfoProgress.pixelInset.height);
		TextInfo.text = text.ToUpper();
	}
	
	
	private IEnumerator fadeToAlpha(float alpha) {
		float timer = 0;
		float curAlpha = InfoBG.color.a;
		
		while(timer < 1f) {
			TextInfo.material.color = Color.Lerp(new Color(1, 1, 1, curAlpha), new Color(1, 1, 1, alpha), timer);
			InfoBG.color = Color.Lerp(new Color(1, 1, 1, curAlpha), new Color(1, 1, 1, alpha), timer);
			InfoProgress.color = Color.Lerp(new Color(1, 1, 1, curAlpha), new Color(1, 1, 1, alpha), timer);
			
			timer += Time.deltaTime * 2f;
			yield return null;
		}
	}
}
