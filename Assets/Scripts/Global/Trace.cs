using UnityEngine;
using System.Collections;

public class Trace : MonoBehaviour {
	public GUIText TextTrace;
	public GUIText TextTraceStatus;
	public GUITexture TraceBG;
	public GUITexture TraceProgress;
	public Material SkyboxMat;
	public Vignetting VignettTarget;
	public NoiseAndGrain NoiseTarget;
	public Color AlertColor;
	public float maxTraceBarLength = 293;
	
	private float traceTime = 3f;
	private bool isTracing = false;
	private Color tempCol;
	
	
	void Awake() {
		tempCol = SkyboxMat.GetColor("_Tint");
		StopTracing();
	}
	
	
	void OnDisable() {
		SkyboxMat.SetColor("_Tint", tempCol);
	}
	
	
	public void StartTracing() {
		if(isTracing)
			return;
		
		isTracing = true;
		TextTrace.text = "";
		TextTraceStatus.text = "0%";
		
		TraceProgress.pixelInset = new Rect(TraceProgress.pixelInset.x, TraceProgress.pixelInset.y, 0, 8);
		TextTrace.enabled = true;
		TextTraceStatus.enabled = true;
		TraceBG.enabled = true;
		TraceProgress.enabled = true;
		
		StartCoroutine(startTracing());
	}
	
	
	public void StopTracing() {
		isTracing = false;
		
		TextTrace.enabled = false;
		TextTraceStatus.enabled = false;
		TraceBG.enabled = false;
		TraceProgress.enabled = false;
		
		NoiseTarget.strength = 0;
		SkyboxMat.SetColor("_Tint", tempCol);
		VignettTarget.chromaticAberration = 0;
		TextTraceStatus.text = "100%";
	}
	
	
	private IEnumerator startTracing() {
		// 5 lines long
		string randomText = "asdashjd kjasdha s dhas ahjskda kjashdas qwh hjw dlas\nas djhja skjas hdkas as dh as dsa dasds gbgnfhj a\nas dghas hjakjs dhfje  fj\nfkjs dfhkds dskj ds dhfjsdhs sf ds\ndhksfdkjsfdkjs hdkjs\nhj ashkakja djas jkl askjas\nhjas djasdhs hjaskjds jkfd  hs dhj dfkjdshds\nhg vfhv dsfs sfsfs\nbgvhkl dsfskjsdhqawhjd jaopaw oawpodwiwopdow ipoaw\nsdjefsfuew foihje fe i esf\nkjaskjkjas as dkjash askjd as\nkja dhsas\nhfsda\n\nTRACED";
		float timer = 0;
		int maxChars = randomText.Length;
		int stringStart = 0;
		
		NoiseTarget.strength = 5;
		
		traceTime = Random.Range(2, 5);
		
		while(timer <= 1f && isTracing) {
			TraceProgress.pixelInset = new Rect(TraceProgress.pixelInset.x, TraceProgress.pixelInset.y, maxTraceBarLength * timer, 8);
			VignettTarget.chromaticAberration = Random.Range (-30f, 30f);
			SkyboxMat.SetColor("_Tint", AlertColor);
			
			int length = (int)((maxChars) * timer);
			
			string subString = randomText.Substring(0, length);
			string[] tempstr = subString.Split('\n');
			
			// string too long, cut off back
			if(tempstr.Length > 5) {
				subString = "";
				
				for(int i = 5; i > 0; i--) {
					subString += tempstr[tempstr.Length - i];
					
					if(i != 1)
						subString += "\n";
				}
			}
			
			TextTrace.text = subString;
			
			timer += Time.deltaTime / traceTime;
			TextTraceStatus.text = (int)(timer * 100) + "%";
			yield return null;
		}
		
		if(isTracing) {
			GameManager.ins.DetectPlayer();
			StartCoroutine(playerDetected());
		} else {
			StopTracing();
		}
	}
	
	private IEnumerator playerDetected() {
		while(isTracing) {
			VignettTarget.chromaticAberration = Random.Range (-30f, 30f);
			
			yield return null;
		}
	}
}
