using UnityEngine;
using System.Collections;

public class EnemyControl : Enemy {
	public Weapon Gun;
	
	private ModelAnimation modelAnim;
	private bool canShoot = false;
	
	
	void Awake() {
		modelAnim = GetComponent<ModelAnimation>();
		Invoke ("enableShoot", Random.Range(0.8f, 1.2f));
	}
	
	
	void OnTriggerEnter(Collider col) {
		if(col.tag == "Player") {
			Debug.Log ("ALERT: VIRUS DETECTED!");
		}
	}
	
	
	void Update() {
		if(Vector3.Distance(transform.position, GameManager.ins.PlayerPtr.position) < 90) {
			if(modelAnim.Status != ModelAnimation.AlertStatus.ALERTED)
				modelAnim.Status = ModelAnimation.AlertStatus.ALERTED;
			
			if(canShoot && GameManager.ins.PlayerAlive && GameManager.ins.GameStarted) {
				shootPlayer();
			}
			
			if(Vector3.Distance(transform.position, GameManager.ins.PlayerPtr.position) > 10)
				transform.Translate((GameManager.ins.PlayerPtr.position - transform.position) / 5f * Time.deltaTime, Space.World);
		} else {
			if(modelAnim.Status != ModelAnimation.AlertStatus.CALM)
				modelAnim.Status = ModelAnimation.AlertStatus.CALM;
		}
	}
	
	
	private void shootPlayer() {
		canShoot = false;
		Gun.FireMahLaser();
		
		Invoke ("enableShoot", Random.Range(0.8f, 1.2f));
	}
	
	
	private void enableShoot() {
		canShoot = true;
	}
}
