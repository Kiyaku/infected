using UnityEngine;
using System.Collections;

public class ModelAnimation : MonoBehaviour {
	public enum AlertStatus {
		CALM,
		ALERTED
	}
	
	public Transform CoreInside;
	public Renderer Enemyshine;
	public Color ColorCalm;
	public Color ColorAlert;
	[HideInInspector]
	public AlertStatus Status = AlertStatus.CALM;
	
	
	void Update() {
		switch(Status) {
			case AlertStatus.CALM:
				transform.Rotate(Vector3.up * 50f * Time.deltaTime);
				transform.localScale = Vector3.one + Vector3.one * (SimplexNoise.Noise.Generate(Time.time * 1f) / 5f);
				CoreInside.Rotate(Vector3.up * 100f * Time.deltaTime);
				StartCoroutine(fadeColors(ColorCalm));
				break;
			
			case AlertStatus.ALERTED:
				transform.Rotate(Vector3.up * 400f * Time.deltaTime);
				transform.localScale = Vector3.one + Vector3.one * (SimplexNoise.Noise.Generate(Time.time * 3f) / 3f);
				CoreInside.Rotate(Vector3.up * 200f * Time.deltaTime);
				StartCoroutine(fadeColors(ColorAlert));
				break;
		}
	}
	
	
	private IEnumerator fadeColors(Color newCol) {
		float timer = 0f;
		Color tempCol = Enemyshine.material.GetColor("_TintColor");
		
		while(timer < 1f) {
			// TODO: Figure out why it's not fading...
			Enemyshine.material.SetColor("_TintColor", Color.Lerp(tempCol, newCol, timer));
			
			timer += Time.deltaTime;
			yield return null;
		}
	}
}
