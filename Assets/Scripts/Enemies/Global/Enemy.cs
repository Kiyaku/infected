using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public int Health;
	public int Damage;
	
	
	void Start() {
		EnemyManager.ins.RegisterEnemy(this);
	}
	
	
	public void DealDamage(int damage) {
		GameManager.ins.PlayHitSound();
		Health -= damage;
		
		CheckHealth();
	}
	
	
	public void CheckHealth() {
		if(Health <= 0) {
			EnemyManager.ins.RemoveEnemy(this);
			
			Destroy (gameObject);
		}
	}
}

