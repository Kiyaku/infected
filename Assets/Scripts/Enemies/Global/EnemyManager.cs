using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {
	public static EnemyManager ins;
	
	public List<Enemy> EnemyList = new List<Enemy>();
	
	
	void Awake() {
		ins = this;
	}
	
	
	public void RegisterEnemy(Enemy enemy) {
		EnemyList.Add(enemy);
	}
	
	
	public void RemoveEnemy(Enemy enemy) {
		EnemyList.Remove(enemy);
	}
}

