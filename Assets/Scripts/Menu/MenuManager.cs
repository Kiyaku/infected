using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {
	public Texture2D MainmenuTex;
	public Texture2D Instructions;
	public Texture2D MainButton;
	public Texture2D InstructionsButton;
	public Texture2D StartButton;
	public Texture2D RestartButton;
	public Texture2D GameOverScreen;
	
	
	private bool showInstructions = false;
	
	
	void OnGUI() {
		if(!GameManager.ins.GameStarted) {
			if(showInstructions) {
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Instructions);
				
				if(GUI.Button(new Rect(Screen.width - (MainButton.width + 10f), 20f, MainButton.width, MainButton.height), MainButton, "Label")) {
					showInstructions = false;
				}
			} else {
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), MainmenuTex);
				
				if(GUI.Button(new Rect(Screen.width / 2f - (StartButton.width / 2f), Screen.height / 2f - 50, StartButton.width, StartButton.height), StartButton, "Label")) {
					Screen.lockCursor = true;
					GameManager.ins.GameStarted = true;
				}
				
				if(GUI.Button(new Rect(Screen.width / 2f - (InstructionsButton.width / 2f), Screen.height / 2f + 80, InstructionsButton.width, InstructionsButton.height), InstructionsButton, "Label")) {
					showInstructions = true;
				}
			}
		} else if(!GameManager.ins.PlayerAlive) {
			Screen.lockCursor = false;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), GameOverScreen);
				
			if(GUI.Button(new Rect(Screen.width / 2f - (RestartButton.width / 2f), Screen.height / 2f + 80, RestartButton.width, RestartButton.height), RestartButton, "Label")) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}
}
