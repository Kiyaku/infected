using UnityEngine;
using System.Collections;

public class EnemyLaserBullet : Bullet {
	public float BulletSpeed = 100f;
	
	IEnumerator Start() {
		GameManager.ins.PlayShotSound();
		transform.LookAt(GameManager.ins.PlayerPtr);
		transform.Rotate(Vector3.up * 90);
		transform.parent = null;
		
		float timer = 0;
		
		while(timer < 2f) {
			transform.Translate(transform.right * -BulletSpeed * Time.deltaTime, Space.World);
			
			timer += Time.deltaTime;
			yield return null;
		}
		
		Destroy(gameObject);
	}
	
	
	void OnTriggerEnter(Collider col) {
		if(col.tag == "Player") {
			col.GetComponent<PlayerControl>().DealDamage(DamagePerHit);
			Destroy (gameObject);
		}
	}
}
