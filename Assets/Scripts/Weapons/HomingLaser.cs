using UnityEngine;
using System.Collections;

public class HomingLaser : Bullet {
	public float BulletSpeed = 20f;
	private Vector3 tempDir;
	
	void Start() {
		transform.LookAt(transform.parent.position);
		tempDir = transform.parent.position - new Vector3(Camera.main.transform.position.x, transform.parent.position.y, Camera.main.transform.position.z);
		transform.parent = null;
	}
	
	
	public void ShootBullet(Vector3 targetPos) {
		StartCoroutine(homeTarget(targetPos));
	}
	
	
	private IEnumerator homeTarget(Vector3 targetPos) {
		float timer = 0; 
				
		while(timer < 3f && Vector3.Distance(transform.position, targetPos) > 0.5f) {
		Vector3 dir = (targetPos - transform.position).normalized;
			Quaternion newRot = Quaternion.LookRotation(dir);
			transform.rotation = Quaternion.Lerp(transform.rotation, newRot, timer);
			
			transform.Translate(Vector3.forward * Time.deltaTime * BulletSpeed);
			timer += Time.deltaTime;
			
			yield return null;
		}
		
		Destroy(gameObject);
	}
	
	
	void OnTriggerEnter(Collider col) {
		if(col.tag == "Firewall") {
			col.GetComponent<Firewall>().DealDamage(DamagePerHit);
		}
		
		if(col.tag == "Enemy") {
			col.GetComponent<Enemy>().DealDamage(DamagePerHit);
		}
		
		Destroy (gameObject);
	}
}
