using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public enum DamageType {
		LASER
	}
	
	public DamageType Type;
	public int DamagePerHit;
}
