using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyPulse : Weapon {
	public List<Transform> GunPoints = new List<Transform>();
	
	
	public override void FireMahLaser() {
		StartCoroutine(fireMahLaser());
	}
	
	
	private IEnumerator fireMahLaser() {
		foreach(Transform point in GunPoints) {
			GameObject bullet = (GameObject)Instantiate (BulletPrefab, point.position, Quaternion.identity);

			yield return new WaitForSeconds(0.05f);
		}
	}
}
