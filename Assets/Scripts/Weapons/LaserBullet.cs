using UnityEngine;
using System.Collections;

public class LaserBullet : Bullet {
	public float BulletSpeed = 100f;
	
	IEnumerator Start() {
		GameManager.ins.PlayShotSound();
		transform.Rotate(Vector3.up * 90);
		transform.parent = null;
		
		float timer = 0;
		
		while(timer < 1f) {
			transform.Translate(transform.right * BulletSpeed * Time.deltaTime, Space.World);
			
			timer += Time.deltaTime * 2f;
			yield return null;
		}
		
		Destroy(gameObject);
	}
	
	
	void OnTriggerEnter(Collider col) {
		if(col.tag == "Firewall") {
			col.GetComponent<Firewall>().DealDamage(DamagePerHit);
		}
		
		if(col.tag == "Enemy") {
			col.GetComponent<Enemy>().DealDamage(DamagePerHit);
		}
		
		Destroy (gameObject);
	}
}
