using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerLaser : Weapon {
	public GUITexture CrossHair;
	public List<Transform> GunPoints = new List<Transform>();
	
	private Enemy closestEnemy = null;
	
	
	void OnEnable() {
		CrossHair.enabled = true;
		StartCoroutine(aimForEnemies());
	}
	
	
	void OnDisable() {
		if(CrossHair != null)
			CrossHair.enabled = false;
		
		StopAllCoroutines();
	}
	
	
	IEnumerator aimForEnemies() {
		while(true) {
			if(this.enabled) {
				closestEnemy = null;
				float lowestAngle = 180;
				
				foreach(Enemy e in EnemyManager.ins.EnemyList) {
					if(e == null)
						continue;
					
					Vector3 forward = transform.position - Camera.main.transform.position;
					Vector3 enemyPos = e.transform.position - Camera.main.transform.position;
					
					float angle = Vector3.Angle (forward, enemyPos);
					
					if(angle < 25 && angle < lowestAngle && Vector3.Distance(e.transform.position, transform.position) < 80) {
						lowestAngle = angle;
						
						Vector3 screenCoord = Camera.main.WorldToScreenPoint(e.transform.position);
						closestEnemy = e;
						CrossHair.enabled = true;
						CrossHair.pixelInset = new Rect(screenCoord.x - 32, screenCoord.y - 32, 64, 64);
					}
				}
				
				CrossHair.enabled = (closestEnemy != null);
			}
			
			yield return null;
		}
	}
	
	
	public override void FireMahLaser() {
		StartCoroutine(fireMahLaser());
	}
	
	
	private IEnumerator fireMahLaser() {
		if(closestEnemy != null) {
			GameManager.ins.PlayerPtr.GetComponent<PlayerControl>().Energy -= 20f;
			Transform target = closestEnemy.transform;
			
			foreach(Transform point in GunPoints) {
				GameManager.ins.PlayShotSound();
				GameObject bullet = (GameObject)Instantiate (BulletPrefab, point.position, Quaternion.identity);
				bullet.transform.parent = transform;
				bullet.GetComponent<HomingLaser>().ShootBullet(target.position);
				
				yield return new WaitForSeconds(0.05f);
			}
		}
		
		yield return null;
	}
}
