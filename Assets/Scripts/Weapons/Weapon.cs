using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	public enum WeaponType {
		PULSE,
		LASER,
		EXPLOSIVE
	}
	
	public WeaponType Type;
	public GameObject BulletPrefab;
	
	
	virtual protected void Awake() {
		this.enabled = false;
	}
	
	
	virtual public void FireMahLaser() {}
}
