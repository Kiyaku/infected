using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerPulse : Weapon {
	public List<Transform> GunPoints = new List<Transform>();
	public Transform ShieldPtr;
	
	
	public override void FireMahLaser() {
		ShieldPtr.parent = null;
		StartCoroutine(fireMahLaser());
	}
	
	
	private IEnumerator fireMahLaser() {
		ShieldPtr.position = transform.position + new Vector3(Mathf.Sin (Camera.main.transform.eulerAngles.y * Mathf.Deg2Rad) * 0.6f, 0, Mathf.Cos (Camera.main.transform.eulerAngles.y * Mathf.Deg2Rad) * 0.6f);
		ShieldPtr.LookAt(new Vector3(Camera.main.transform.position.x, ShieldPtr.position.y, Camera.main.transform.position.z));
		ShieldPtr.renderer.enabled = true;
		
		GameManager.ins.PlayerPtr.GetComponent<PlayerControl>().Energy -= 1f;
		
		foreach(Transform point in GunPoints) {
			GameObject bullet = (GameObject)Instantiate (BulletPrefab, point.position, ShieldPtr.rotation);

			yield return new WaitForSeconds(0.05f);
		}
		
		ShieldPtr.renderer.enabled = false;
	}
}
