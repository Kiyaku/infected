using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisableMats : MonoBehaviour {
	protected class MatData {
		public Material mat;
		public Color startCol;
	}
	
	public Transform RenderTarget;
	public Color DeactivatedColor;
	
	private List<MatData> matData = new List<MatData>();
	
	
	void Awake() {
		AddMat(RenderTarget);
	}
	
	
	private void AddMat(Transform t) {
		if(t.renderer != null) {
			foreach(Material m in t.renderer.materials) {
				string col = m.HasProperty("_TintColor") ? "_TintColor" : "_Color";
				
				if(m.HasProperty(col)) {
					MatData md = new MatData();
					
					md.mat = m;
					md.startCol = m.GetColor(col);
					matData.Add(md);
				}
			}
		}
	
		foreach(Transform child in t) {
			AddMat(child);
		}
	}
	
	
	public void Deactivate() {
		StartCoroutine(fadeToColor(false));
	}
	
	
	public void Activate() {
		StartCoroutine(fadeToColor(true));
	}
	
	
	private IEnumerator fadeToColor(bool activate) {
		float timer = 0;
		
		while(timer < 0.8f) {
			foreach(MatData md in matData) {
				string col = md.mat.HasProperty("_TintColor") ? "_TintColor" : "_Color";
				
				if(activate) {
					md.mat.SetColor(col, Color.Lerp(DeactivatedColor, md.startCol, timer + 0.2f));
				} else {
					md.mat.SetColor(col, Color.Lerp(md.startCol, DeactivatedColor, timer));
				}
			}
			
			timer += Time.deltaTime * 1.5f;
			yield return null;
		}
	}
}
