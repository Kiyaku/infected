using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {
	public Color NewColor;
	public string ColorName;
	
	
	void Awake() {
		renderer.material.SetColor (ColorName, NewColor);
	}
}
