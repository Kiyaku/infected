using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour {
	public float XSpeed = 0f;
	public float YSpeed = 0f;
	
	public int ColIndex = 0;
	private Material mat;
	
	
	void Awake() {
		mat = renderer.materials[ColIndex];
	}
	
	
	void Update() {
		if(mat != null) {
			mat.SetTextureOffset("_MainTex", new Vector2(XSpeed * Time.time, YSpeed * Time.time));
		}
	}
}
