using UnityEngine;
using System.Collections;

public class Trojan : MonoBehaviour {
	public enum Type {
		HDD,
		SSD
	}
	
	private enum Task {
		GATHER,
		RETURN,
		UNLOAD,
		APPROACH
	}
	
	public string Name;
	public Type TrojanType = Type.HDD;
	public Transform MyTarget;
	public Transform MyDrive;
	public Transform Model;
	
	private Task task = Task.GATHER;
	private int storedData = 0;
	private float movementSpeed = 10f;
	private float angle = 0;
	
	
	void Start() {
		StartCoroutine(process());
	}
	
	
	void Update() {
		Model.Rotate(Vector3.up * 50f * Time.deltaTime);
		Model.position = transform.position + new Vector3(0, Mathf.Sin ((angle / 2f) * Mathf.Deg2Rad) / 2f, Mathf.Sin (angle * Mathf.Deg2Rad) / 2f);
		angle += Time.deltaTime * 260f;
	}
	
	
	private IEnumerator process() {
		float timer = 0;
		Vector3 tPos;
		
		while(true) {
			switch(task) {
				case Task.GATHER:
					timer = 0;
					int amount = MyTarget.GetComponent<Node>().DataOverTime;
					int interval = MyTarget.GetComponent<Node>().DataInterval;
				
					while(timer < interval) {
						timer += Time.deltaTime;
						yield return null;
					}
				
					storedData = amount;
					task = Task.RETURN;
					break;
				
				case Task.RETURN:
					tPos = new Vector3(MyDrive.position.x, 20, MyDrive.position.z);
					while(Vector3.Distance(transform.position, tPos) > 5f) {
						transform.LookAt(tPos);
						transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);
						yield return null;
					}
				
					task = Task.UNLOAD;
					break;
				
				case Task.UNLOAD:
					timer = 0;
				
					while(timer < 5f) {
						timer += Time.deltaTime;
						yield return null;
					}
					
					GameManager.ins.GatherData(storedData);
					storedData = 0;
					task = Task.APPROACH;
					break;
				
				case Task.APPROACH:
					tPos = new Vector3(MyTarget.position.x, 20, MyTarget.position.z);
					while(Vector3.Distance(transform.position, tPos) > 5f) {
						transform.LookAt(tPos);
						transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);
						yield return null;
					}
				
					task = Task.GATHER;
					break;
			}
			
			yield return null;
		}
	}
}
