using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : MonoBehaviour {
	[System.Serializable]
	public class PlayerBehaviour {
		public PlayerModes Mode;
		public float RotationSpeed;
		public Color HighlightColor;
		public Weapon BehaviourGun;
		public float MovementSpeed;
		public float ShieldMod;
	}
	
	public float PlayerSpeed = 1f;
	public float RotationSpeed = 10f;
	public float ManeveurSpeed = 3f;
	public float CamDistance = 7f;
	public Transform PlayerCam;
	public Transform PlayerModel;
	public Light PlayerLight;
	public List<PlayerBehaviour> BehaviourList = new List<PlayerBehaviour>();
	public Weapon MyGun;
	public Transform LightColumn;
	public GUITexture HealthBar;
	public GUITexture EnergyBar;
	public GUIText HealthBarText;
	public GUIText EnergyBarText;
	public float Health;
	public float Energy;
	
	private Vector3 moveDir;
	private float rotAngle = 180;
	private Material highlightMat;
	private PlayerModes currentPlayerMode = PlayerModes.NEUTRAL;
	private PlayerBehaviour currentPlayerBehaviour;
	private float shieldModifier = 1f;
	
	private bool isAlive = true;
	
	
	public enum PlayerModes {
		NEUTRAL,
		AGGRESSIVE,
		FRIENDLY
	}
	
	
	void Start() {
		MyGun.enabled = true;
		currentPlayerBehaviour = BehaviourList[0];
		highlightMat = PlayerModel.renderer.material;
		StartCoroutine(movePlayer());
		StartCoroutine(animatePlayer());
		StartCoroutine(switchModes());		
	}
	
	
	private IEnumerator movePlayer() {
		while(isAlive) {
			if(GameManager.ins.GameStarted) {				
				// Movement
				moveDir.z += Input.GetAxis("Vertical")  * Time.deltaTime * ManeveurSpeed;
				moveDir.x += Input.GetAxis("Horizontal") * Time.deltaTime * ManeveurSpeed;
				moveDir = Vector3.Lerp (moveDir, Vector3.zero, Time.deltaTime);
				
				// Rotation
				rotAngle -= Input.GetAxis("Mouse X") * Time.deltaTime * RotationSpeed;
				
				Vector3 tempDir = transform.position - new Vector3(PlayerCam.position.x, transform.position.y, PlayerCam.position.z);
				
				PlayerCam.position = transform.position + (new Vector3(Mathf.Cos(rotAngle * Mathf.Deg2Rad), 0.3f, Mathf.Sin(rotAngle * Mathf.Deg2Rad)) * CamDistance);
				PlayerCam.LookAt(transform.position + new Vector3(0, 1, 0));
				
				Quaternion tempRot = Quaternion.LookRotation(tempDir);
				transform.rotation = Quaternion.Slerp(transform.rotation, tempRot, Time.deltaTime);
				
				Debug.DrawRay(transform.position, tempDir, Color.red);
				
				transform.Translate(moveDir * currentPlayerBehaviour.MovementSpeed * Time.deltaTime);
				PlayerModel.position = transform.position;
				PlayerModel.Rotate(Vector3.up * currentPlayerBehaviour.RotationSpeed * Time.deltaTime);
				
				// Temp
				if(Input.GetKeyDown("1")) {			
					currentPlayerBehaviour = BehaviourList[0];
				}
				
				if(Input.GetKeyDown("2")) {			
					currentPlayerBehaviour = BehaviourList[1];
				}
				
				if(Input.GetKeyDown("3")) {			
					currentPlayerBehaviour = BehaviourList[2];
				}
				
				if(Input.GetMouseButtonDown(0)) {
					if(MyGun != null && Energy > ((currentPlayerMode == PlayerModes.AGGRESSIVE) ? 20f : 1f)) {
						MyGun.FireMahLaser();
					}
				}
				
				if(Input.GetMouseButtonDown(1)) {
					StartCoroutine(injectTrojan());
				}
				
				regenerate();
			}
			
			yield return null;
		}
	}
	
	
	private void regenerate() {
		Health += 0.1f;
		Energy += 0.1f;
		
		if(Health > 100) 
			Health = 100;
		
		if(Energy > 100) 
			Energy = 100;
		
		updateHealthBar();
	}
	
	
	private IEnumerator injectTrojan() {
		Transform node = null;
		Ray ray;
		RaycastHit hit;
		
		Transform column = (Transform)Instantiate (LightColumn);
		
		while(Input.GetMouseButton(1)) {
			Vector3 tempDir = transform.position - new Vector3(PlayerCam.position.x, transform.position.y, PlayerCam.position.z);
			ray = new Ray(transform.position, tempDir.normalized);
			
			column.position = Vector3.one * 10000;
			
			node = null;
			
			if(Physics.Raycast(ray, out hit, 200)) {
				if(hit.transform.tag == "Node") {
					Vector3 tempPos = hit.transform.position + (transform.position - hit.transform.position).normalized * 10f;
					tempPos.y = 0;
					column.position = tempPos;
					node = hit.transform;
				}
			}
			
			yield return null;
		}
		
		if(node != null) {
			Vector3 tempPos = node.position + (transform.position - node.position).normalized * 10f;
			tempPos.y = 20;
			GameManager.ins.SpawnTrojan(node);
		}
		
		Destroy(column.gameObject);
	}
	
	
	private IEnumerator animatePlayer() {
		float angle = 0;
		
		while(true) {
			switch(currentPlayerMode) {
				case PlayerModes.NEUTRAL:
					PlayerModel.position = transform.position + new Vector3(0, Mathf.Sin (angle * Mathf.Deg2Rad) / 4f, 0);
					angle += Time.deltaTime * 25f;
					angle %= 360;
					break;
				
				case PlayerModes.AGGRESSIVE:
					float shake = SimplexNoise.Noise.Generate(Time.deltaTime / 10f) * 4f;
					PlayerModel.position = transform.position + new Vector3(Random.Range(-shake, shake), Random.Range(-shake, shake), Random.Range(-shake, shake));
					break;
				
				case PlayerModes.FRIENDLY:
					PlayerModel.position = transform.position + new Vector3(0, Mathf.Sin ((angle / 2f) * Mathf.Deg2Rad) / 2f, Mathf.Sin (angle * Mathf.Deg2Rad) / 2f);
					angle += Time.deltaTime * 260f;
					break;
			}
			
			yield return null;
		}
	}
	
	
	private IEnumerator switchModes() {
		PlayerBehaviour tempBehaviour = null;
		
		while(true) {
			while(tempBehaviour == currentPlayerBehaviour) 
				yield return null;
			
			tempBehaviour = currentPlayerBehaviour;
			StartCoroutine(fadeToColor(tempBehaviour.HighlightColor));
			currentPlayerMode = tempBehaviour.Mode;
			shieldModifier = tempBehaviour.ShieldMod;
			
			if(MyGun != null)
				MyGun.enabled = false;
			
			MyGun = tempBehaviour.BehaviourGun;
			
			if(MyGun != null)
				MyGun.enabled = true;
			
			yield return null;
		}
	}
	
	
	public void DealDamage(int damage) {
		GameManager.ins.PlayHitSound();
		Health -= damage * shieldModifier;
		updateHealthBar();
		
		CheckHealth();
	}
	
	
	public void CheckHealth() {
		if(Health <= 0) {
			isAlive = false;
			GameManager.ins.PlayerAlive = false;
		}
	}
	
	
	private void updateHealthBar() {
		if(Energy < 0)
			Energy = 0;
		
		if(Health < 0)
			Health = 0;
		
		HealthBarText.text = "HEALTH " + (int)Health + "%";
		EnergyBarText.text = "ENERGY " + (int)Energy + "%";
		HealthBar.pixelInset = new Rect(HealthBar.pixelInset.x, HealthBar.pixelInset.y, (Health / 100f) * 228f, HealthBar.pixelInset.height);
		EnergyBar.pixelInset = new Rect(EnergyBar.pixelInset.x, EnergyBar.pixelInset.y, (Energy / 100f) * -228f, EnergyBar.pixelInset.height);
	}
	
	
	private IEnumerator fadeToColor(Color col) {
		float timer = 0;
		Color tempCol = highlightMat.color;
		
		while(timer < 1f) {
			highlightMat.SetColor("_Color", Color.Lerp(tempCol, col, timer));
			PlayerLight.color = highlightMat.color;
			
			timer += Time.deltaTime * 2f;
			yield return null;
		}
	}
	
	
	void OnTriggerEnter(Collider col) {
		switch(col.tag) {
			case "Node":
				GameManager.ins.ShowNodeInterface(col.GetComponent<Node>());
				break;
			
			case "Drive":
				GameManager.ins.ShowDriveInterface(col.GetComponent<HardDrive>());
				break;
		}
	}
}
