using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour {
	public bool IsActive = false;
	
	
	virtual public void Awake() {
		Disable();
	}
	
	
	virtual public void Activate() {
		IsActive = true;
		
		if(transform.GetComponent<DisableMats>() != null) {
			transform.GetComponent<DisableMats>().Activate();
		}
	}
	
	
	virtual public void Disable() {
		IsActive = false;
		
		if(transform.GetComponent<DisableMats>() != null) {
			transform.GetComponent<DisableMats>().Deactivate();
		}
	}
}
