using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeManager : MonoBehaviour {
	public static NodeManager ins;
	
	public Transform NodePathPrefab;
	public List<Node> NodeList = new List<Node>();
	
	private List<Transform> nodePathList = new List<Transform>();
	
	
	void Awake() {
		ins = this;
		
		for(int i = 0; i < NodeList.Count; i++) {
			nodePathList.Add((Transform)Instantiate (NodePathPrefab));
			nodePathList[i].GetChild(0).renderer.enabled = false;
		}
		
		StartCoroutine(animatePath());
	}
	
	
	public void AddNode(Node node) {
		NodeList.Add(node);
	}
	
	
	public bool IsNodeAvailable(Node node) {
		foreach(Node n in NodeList) {
			if(n == node) 
				return true;
			
			if(!n.IsActive)
				return false;
		}
		
		return false;
	}
	
	
	public void UnlockNodes(Node n) {
		int index = NodeList.IndexOf(n);
		
		for(int i = index + 1; i < NodeList.Count; i++) {
			NodeList[i].UnlockNode();
		}
	}
	
	
	private IEnumerator animatePath() {
		float timer = 0;
		
		while(true) {
			for(int i = 0; i < NodeList.Count - 1; i++) {
				if(NodeList[i].IsActive) {
					nodePathList[i].GetChild(0).renderer.enabled = false;
				} else {
					nodePathList[i].GetChild(0).renderer.enabled = true;
					
					nodePathList[i].LookAt(NodeList[i + 1].transform);
					nodePathList[i].position = Vector3.Lerp(NodeList[i].transform.position + new Vector3(0, 3, 0), NodeList[i + 1].transform.position + new Vector3(0, 3, 0), timer);
					nodePathList[i].GetChild(0).renderer.material.SetColor("_TintColor", new Color(1, 1, 1, 1f - (Mathf.Abs((timer - 0.5f)) * 2f)));
				}
			}
			
			timer += Time.deltaTime;
			
			if(timer > 1f) 
				timer -= 1f;
			
			yield return null;
		}
	}
}
