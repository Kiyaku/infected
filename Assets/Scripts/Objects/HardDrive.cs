using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HardDrive : Building {
	public bool Hacked = false;
	public List<Renderer> AnimatedRenderer = new List<Renderer>();
	
	private List<Color> rendererColors = new List<Color>();
	
	
	public override void Awake () {
		base.Awake ();
		
		for(int i = 0; i < AnimatedRenderer.Count; i++) {
			rendererColors.Add(AnimatedRenderer[i].material.color);
			AnimatedRenderer[i].material.color *= 0.2f;
		}
	}
	
	
	public void HackDrive() {
		Hacked = true;
		GameManager.ins.AddHardDrive(this);
		
		StartCoroutine(animateColors());
	}
	
	
	private IEnumerator animateColors() {
		float timer = 0;
		float maxTime = 1;
		float offset = 0;
		
		while(Hacked) {
			while(timer < maxTime) {
				for(int i = 0; i < AnimatedRenderer.Count; i++) {
					AnimatedRenderer[i].material.color = Color.Lerp(AnimatedRenderer[i].material.color, new Color(rendererColors[i].r + offset, rendererColors[i].g + offset, rendererColors[i].b + offset, 1), timer);
				}
				
				timer += Time.deltaTime * 10f;
				yield return null;
			}
			
			offset = Random.Range(-0.9f, 0);
			maxTime = Random.Range(0.3f, 1f);
			timer = 0;
			
			yield return null;
		}
	}
}
