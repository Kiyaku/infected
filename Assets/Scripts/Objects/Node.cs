using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour {
	[System.Serializable]
	public class PowerColor {
		public PowerStatus Status;
		public Color Col;
	}
	
	public enum PowerStatus {
		OFFLINE,
		ONLINE
	}
	
	public PowerStatus NodeStatus = PowerStatus.OFFLINE;
	public List<Building> Buildings = new List<Building>();
	public List<PowerColor> PowerColors = new List<PowerColor>();
	public Renderer ColorRenderer;
	public int ColIndex = 0;
	public List<ElectroLine> ElectroEmitter = new List<ElectroLine>();
	public bool IsActive = false;
	public Transform NodePathPrefab;
	public int InitialData = 10;
	public int DataOverTime = 1;
	public int DataInterval = 10;
	
	private Material NodeMaterial;
	private List<Transform> nodePathList = new List<Transform>();
	
	
	void Start() {
		NodeMaterial = ColorRenderer.materials[ColIndex];
		
		for(int i = 0; i < Buildings.Count; i++) {
			nodePathList.Add((Transform)Instantiate (NodePathPrefab));
			nodePathList[i].GetChild(0).renderer.enabled = false;
		}
		
		StartCoroutine(animatePath());
		
		if(IsActive)
			ActivateNode();
		else if(GetComponent<DisableMats>() != null)
			GetComponent<DisableMats>().Deactivate();
	}
	
	
	public void UnlockNode() {
		if(GetComponent<DisableMats>() != null)
			GetComponent<DisableMats>().Activate();
	}
	
	
	public void ActivateNode() {
		NodeManager.ins.UnlockNodes(this);
		StartCoroutine(switchState(PowerStatus.ONLINE));
		
		IsActive = true;
		
		audio.Play ();
		
		GameManager.ins.GatherData(InitialData);
		
		foreach(ElectroLine el in ElectroEmitter) 
			el.StartEmitting();
		
		foreach(Building b in Buildings) {
			b.Activate();
		}
	}
	
	
	IEnumerator switchState(PowerStatus newState) {
		Color oldCol = getColor(NodeStatus);
		Color newCol = getColor(newState);
		float timer = 0;
		
		while(timer < 1) {
			NodeMaterial.SetColor("_Color", Color.Lerp(oldCol, newCol, timer));
			
			timer += Time.deltaTime;
			yield return null;
		}
		
		NodeStatus = newState;
	}
	
	
	private Color getColor(PowerStatus status) {
		foreach(PowerColor pc in PowerColors) {
			if(pc.Status == status) 
				return pc.Col;
		}
		
		return Color.white;
	}
	
	
	private IEnumerator animatePath() {
		float timer = 0;
		
		while(true) {
			for(int i = 0; i < Buildings.Count; i++) {
				if(Buildings[i].IsActive) {
					nodePathList[i].GetChild(0).renderer.enabled = false;
				} else {
					nodePathList[i].GetChild(0).renderer.enabled = true;
					
					nodePathList[i].LookAt(Buildings[i].transform);
					nodePathList[i].position = Vector3.Lerp(transform.position + new Vector3(0, 3, 0), Buildings[i].transform.position + new Vector3(0, -3, 0), timer);
					nodePathList[i].GetChild(0).renderer.material.SetColor("_TintColor", new Color(1, 1, 1, 1f - (Mathf.Abs((timer - 0.5f)) * 2f)));
				}
			}
			
			timer += Time.deltaTime;
			
			if(timer > 1f) 
				timer -= 1f;
			
			yield return null;
		}
	}
}
