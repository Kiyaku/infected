using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Firewall : Building {
	public Transform Core;
	public Color AngryColor;
	public Color HitColor;
	public Transform AntiVirusPrefab;
	public List<Transform> PlatformList = new List<Transform>();
	public int Health = 2000;
	public Transform MyShield;
	
	private Color originalColor; 
	private bool isActive = true;
	private List<Transform> myAntiVirus = new List<Transform>();
	private Vector3 startPos;
	
	
	void Start() {
		for(int i = 0; i < 10; i++) 
			myAntiVirus.Add(null);
		
		startPos = Core.position;

		originalColor = Core.renderer.material.color;
		StartCoroutine(animateCore());
		
		toggleShields(true);
	}
	
	
	public override void Activate () {
		base.Activate ();
		
		StartCoroutine(deactiveMyShield());
	}
	
	
	private IEnumerator deactiveMyShield() {
		float timer = 0;
		Color tempCol = Color.white;
		
		while(timer < 1f) {
			if(timer < 0.5f) {
				tempCol = MyShield.renderer.material.GetColor("_TintColor");
				tempCol.a = Random.Range(0.1f, 0.3f);
				MyShield.renderer.material.SetColor ("_TintColor", tempCol);
			} else {
				tempCol.a = Mathf.Lerp(0.3f, 0, timer);
				MyShield.renderer.material.SetColor ("_TintColor", tempCol);
			}
			
			timer += Time.deltaTime / 2f;
			yield return null;
		}
		
		MyShield.collider.enabled = false;
	}
	
	
	private void toggleShields(bool active) {
		foreach(Transform t in PlatformList)
			t.GetChild(0).gameObject.SetActiveRecursively(active);
	}
	
	
	public void DealDamage(int damage) {
		if(!isActive)
			return;
		
		GameManager.ins.PlayHitSound();
		
		Health -= damage;
		
		StartCoroutine(hitCore());
		
		CheckHealth();
	}
	
	
	public void CheckHealth() {
		if(Health <= 0) {
			audio.Play();
			isActive = false;
			
			StopAllCoroutines();
			StartCoroutine(deactivateShields());
			StartCoroutine(deactivedFirewall());
		}
	}
	
	
	private IEnumerator deactivateShields() {
		float timer = 0;
		Color tempCol = Color.white;
		
		while(timer < 1f) {
			foreach(Transform t in PlatformList) {
				if(timer < 0.5f) {
					tempCol = t.GetChild(0).renderer.material.GetColor("_TintColor");
					tempCol.a = Random.Range(0.1f, 0.3f);
					t.GetChild(0).renderer.material.SetColor ("_TintColor", tempCol);
				} else {
					tempCol.a = Mathf.Lerp(0.3f, 0, timer);
					t.GetChild(0).renderer.material.SetColor ("_TintColor", tempCol);
				}
			}
			
			timer += Time.deltaTime / 2f;
			yield return null;
		}
			
		toggleShields(false);
	}
	
	
	private IEnumerator deactivedFirewall() {
		float timer = 0;
		
		Vector3 oldPos = Core.position;
		Vector3 endPos = startPos - new Vector3(0, 5, 0);
		
		while(timer < 1) {
			Core.position = Vector3.Lerp(oldPos, endPos, timer);
			
			if(timer < 0.5f)
				Core.renderer.material.color = Color.Lerp(originalColor, Color.black, timer * 2f);
			
			timer += Time.deltaTime;
			yield return null;
		}
	}
	
	
	private IEnumerator hitCore() {
		float timer = 0f;
		
		Core.renderer.material.color = HitColor;
		
		while(timer < 1f) {
			Core.renderer.material.color = Color.Lerp(HitColor, originalColor, timer);
			
			timer += Time.deltaTime * 2f;
			yield return null;
		}
	}
	
	
	private IEnumerator animateCore() {
		float angle = 0;
		float shakeAmount = 0;
		float shakeSpeed = 0;
		float shake = 0;
		
		while(true) {
			shake = SimplexNoise.Noise.Generate(Time.deltaTime * shakeSpeed) * shakeAmount;
			Core.position = startPos + new Vector3(Random.Range(-shake, shake), Random.Range(-shake, shake), Random.Range(-shake, shake));
			Core.position += new Vector3(0, Mathf.Sin (angle * Mathf.Deg2Rad) * 2f, 0);
			angle += Time.deltaTime * 125f;
			angle %= 360;
			
			shakeSpeed += Time.deltaTime / 5f;
			shakeAmount = shakeSpeed * 10f;
			
			if(shakeSpeed > 1) {
				StartCoroutine(spawnAntiVirus());
				shakeSpeed -= 1f;
			}
			
			yield return null;
		}
	}
	
	
	private IEnumerator spawnAntiVirus() {
		float timer = 0; 
		
		while(timer < 1) {
			Core.renderer.material.color = Color.Lerp(originalColor, AngryColor, timer);
			
			timer += Time.deltaTime * 5f;
			
			yield return null;
		}
		
		int spaceLeft = 0;
		
		foreach(Transform t in myAntiVirus) {
			if(t == null)
				spaceLeft++;
		}
		
		if(spaceLeft > 0) {
			int maxCount = Random.Range(2, 4);
			
			if(maxCount > spaceLeft) {
				maxCount = spaceLeft;
			}
			
			for(int i = 0; i < maxCount; i++) {
				for(int j = 0; j < myAntiVirus.Count; j++) {
					if(myAntiVirus[j] == null) {
						float angle = (j * 36f) * Mathf.Deg2Rad;
						Vector3 spawnPos = transform.position + new Vector3(Mathf.Sin(angle) * 25f, 3f, Mathf.Cos(angle) * 25f);
				
						myAntiVirus[j] = (Transform)Instantiate(AntiVirusPrefab, spawnPos, Quaternion.identity);
						break;
					}
				}
			}
		}
		
		timer = 0;
		
		while(timer < 1) {
			Core.renderer.material.color = Color.Lerp(AngryColor, originalColor, timer);
			
			timer += Time.deltaTime * 5f;
			
			yield return null;
		}
	}
}
