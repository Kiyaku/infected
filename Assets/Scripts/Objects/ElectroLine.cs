using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ElectroLine : MonoBehaviour {
	public List<Transform> SpawnParents = new List<Transform>();
	public Light LightEmitter;
	
	private LineRenderer lineRenderer;
	
	
	void Start() {
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	
	public void StartEmitting() {
		if(lineRenderer != null)
			StartCoroutine(emitParticle());
	}
	
	
	public void StopEmitting() {
		StopAllCoroutines();
	}
	
	
	IEnumerator emitParticle() {
		yield return new WaitForSeconds(Random.Range (0f, 2f));
		
		lineRenderer.SetVertexCount(4);
		
		while(true) {
			float timer = 0f;
			float offset = 0.2f;
			float maxTime = Random.Range (0.7f, 1.3f);
			int index 			= Random.Range(0, SpawnParents.Count);
			int index2			= index;
			Vector3 startPos 	= getNode(SpawnParents[index]).position;
			
			while(index2 == index) {
				index2 = Random.Range(0, SpawnParents.Count);
			}
			
			Vector3 endPos  = getNode(SpawnParents[index2]).position;
			
			lineRenderer.enabled = true;
			
			while(timer < maxTime) {
				//if(LightEmitter != null) 
				//	LightEmitter.intensity = 5 + Random.Range (-offset, offset) * 4f;
				
				for(int i = 0; i <= 3; i++) {
					lineRenderer.SetPosition(i, Vector3.Lerp(startPos, endPos, i / 3f) + new Vector3(Random.Range (-offset, offset), Random.Range (-offset, offset), Random.Range (-offset, offset)));			
				}
				
				timer += Time.deltaTime;
				
				yield return null;
			}
			
			//if(LightEmitter != null) 
				//LightEmitter.intensity = 0;
				
			lineRenderer.enabled = false;
				
			yield return new WaitForSeconds(Random.Range (1, 3));
		}
	}
	
	
	private Transform getNode(Transform parent) {
		return parent.GetChild(Random.Range (0, parent.childCount));
	}
}
